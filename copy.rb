#!/usr/bin/ruby
# this program copies Vietnamese glyphs from source bdf to target bdf,
# overwriting the old ones if exist. It is used to bring your modified
# glyphs back to source bdf as fontforge-generated bdf is usually too
# different from the source bdf
# Usage: ./copy.rb modified.bdf source.bdf output.bdf
# Once you check that output.bdf is okay, overwrite source.bdf
# with output.bdf
require 'bdf'
a = BDF.new(ARGV[0])
b = BDF.new(ARGV[1])
b.copy_all(a)
b.write(ARGV[1])
