#!/usr/bin/ruby
# this program makes missing Vietnamese glyphs from "standard" glyphs
# so you only need to add accents to complete the glyphs
# Usage: ./dup.rb source.bdf output.bdf
require 'bdf'
a = BDF.new(ARGV[0])
a.dup_all
a.write(ARGV[1])

