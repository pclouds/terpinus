def open_bdf(file)
  f = File.open(file,'r')
  buf = f.readlines.map {|x| x.chomp}
  f.close
  class << buf
    def init
      @line = 0
    end
  
    def line
      self[@line]
    end
  
    def swallow
      @line += 1
    end
  
    def empty
      @line == self.length
    end
  end
  buf.init
  buf
end

class BDF
  attr_reader :glyphs, :header, :glyph_ref
  def initialize(filename = false)
    @glyphs = []
    @glyph_ref = {}
    @header = []
    @footer = []
    if filename.is_a? String
      buf = open_bdf(filename)
      glyph_processing = false
      while (!buf.empty) do
        items = buf.line.split(/ /)
        if items[0] == 'STARTCHAR'
        glyph_processing = true
          g = Glyph.new(buf)
        @glyphs << g
        @glyph_ref[g.name] = g
        else
        glyph_processing ? @footer << buf.line : @header << buf.line
        buf.swallow
        end
      end
    end
    self
  end

  def write(io)
    if io.is_a? String
      f = File.open(io,'w')
      ret = self.write(f)
      f.close
      return ret
    end
    glyphs = @glyph_ref.values.sort {|x,y| x.encoding <=> y.encoding}
    @header.each {|h| io.puts h =~ /^CHARS / ? "CHARS #{glyphs.length}" : h}
    glyphs.each {|g| g.write(io) }
    @footer.each {|h| io.puts h}
    true
  end

  NAMES = {
    0x01A0 => 'Ohorn',
    0x01A1 => 'ohorn',
    0x01AF => 'Uhorn',
    0x01B0 => 'uhorn',
    0x1EF2 => 'Ygrave',
    0x1EF3 => 'ygrave',
  }
  REFS = {
    0x01A0 => 'O',
    0x01A1 => 'o',
    0x01AF => 'U',
    0x01B0 => 'u',
  
    0x1EA0 => 'A',
    0x1EA1 => 'a',
    0x1EA2 => 'A',
    0x1EA3 => 'a',
    
    0x1EA4 => 'Acircumflex',
    0x1EA5 => 'acircumflex',
    0x1EA6 => 'Acircumflex',
    0x1EA7 => 'acircumflex',
    0x1EA8 => 'Acircumflex',
    0x1EA9 => 'acircumflex',
    0x1EAA => 'Acircumflex',
    0x1EAB => 'acircumflex',
    0x1EAC => 'Acircumflex',
    0x1EAD => 'acircumflex',
    
    0x1EAE => 'Abreve',
    0x1EAF => 'abreve',
    0x1EB0 => 'Abreve',
    0x1EB1 => 'abreve',
    0x1EB2 => 'Abreve',
    0x1EB3 => 'abreve',
    0x1EB4 => 'Abreve',
    0x1EB5 => 'abreve',
    0x1EB6 => 'Abreve',
    0x1EB7 => 'abreve',
    
    0x1EBA => 'E',
    0x1EBB => 'e',
    
    0x1EBE => 'Ecircumflex',
    0x1EBF => 'ecircumflex',
    0x1EC0 => 'Ecircumflex',
    0x1EC1 => 'ecircumflex',
    0x1EC2 => 'Ecircumflex',
    0x1EC3 => 'ecircumflex',
    0x1EC4 => 'Ecircumflex',
    0x1EC5 => 'ecircumflex',
    0x1EC6 => 'Ecircumflex',
    0x1EC7 => 'ecircumflex',
    
    0x1EC8 => 'I',
    0x1EC9 => 'i',
    0x1ECA => 'I',
    0x1ECB => 'i',
    
    0x1ECE => 'O',
    0x1ECF => 'o',
    
    0x1ED0 => 'Ocircumflex',
    0x1ED1 => 'ocircumflex',
    0x1ED2 => 'Ocircumflex',
    0x1ED3 => 'ocircumflex',
    0x1ED4 => 'Ocircumflex',
    0x1ED5 => 'ocircumflex',
    0x1ED6 => 'Ocircumflex',
    0x1ED7 => 'ocircumflex',
    0x1ED8 => 'Ocircumflex',
    0x1ED9 => 'ocircumflex',
    
    0x1EDA => 'O',
    0x1EDB => 'o',
    0x1EDC => 'O',
    0x1EDD => 'o',
    0x1EDE => 'O',
    0x1EDF => 'o',
    0x1EE0 => 'O',
    0x1EE1 => 'o',
    0x1EE2 => 'O',
    0x1EE3 => 'o',
    
    0x1EE4 => 'U',
    0x1EE5 => 'u',
    0x1EE6 => 'U',
    0x1EE7 => 'u',
    
    0x1EE8 => 'U',
    0x1EE9 => 'u',
    0x1EEA => 'U',
    0x1EEB => 'u',
    0x1EEC => 'U',
    0x1EED => 'u',
    0x1EEE => 'U',
    0x1EEF => 'u',
    0x1EF0 => 'U',
    0x1EF1 => 'u',
    
    0x1EF2 => 'Y',
    0x1EF3 => 'y',

    0x1EF4 => 'Y',
    0x1EF5 => 'y',
    0x1EF6 => 'Y',
    0x1EF7 => 'y',
  }

  def copy_glyph(name,bdf)
    if bdf.glyph_ref[name].nil?
      STDERR.puts "Glyph #{name} not found"
    else
      g = bdf.glyph_ref[name].dup
      @glyph_ref[g.name] = g
    end
  end

  def copy_glyph_by_id(name,bdf)
    if name.is_a? String
      self.copy_glyph(name,bdf)
    end
    if name.is_a? Array
      name.each {|x| self.copy_glyph_by_id(x,bdf) }
    end
    nil
  end

  def dup_glyph(name,newname,newencoding,bdf = nil)
    g = bdf.nil? ? @glyph_ref[name].dup : bdf.glyph_ref[name].dup
    g.name = newname
    g.encoding = newencoding
    @glyph_ref[g.name] = g
  end

  def dup_glyph_by_id(id,bdf = nil)
    if id.is_a? Fixnum
      return unless REFS.key? id
      name = NAMES[id] || "uni#{id.to_s(16).upcase}"
      self.dup_glyph(REFS[id],name,id,bdf)
    end
    if id.is_a? Array
      id.each {|x| self.dup_glyph_by_id(x,bdf) }
    end
    nil
  end

  def copy_all(bdf)
    self.copy_glyph_by_id(REFS.keys.map {|x| NAMES[x] || "uni#{x.to_s(16).upcase}"} ,bdf)
  end

  def dup_all(bdf = nil)
    #$bdf.dup_glyph_by_id([416, 417, 431, 432, 7922, 7923,
    #0x1EA0, 0x1EA1, 0x1EA2, 0x1EA3,
    #0x1EA4, 0x1EA5, 0x1EA6, 0x1EA7, 0x1EA8, 0x1EA9, 0x1EAA, 0x1EAB, 0x1EAC, 0x1EAD,
    #0x1EAE, 0x1EAF, 0x1EB0, 0x1EB1, 0x1EB2, 0x1EB3, 0x1EB4, 0x1EB5, 0x1EB6, 0x1EB7,
    #0x1EBA, 0x1EBB,
    #0x1EBE, 0x1EBF, 0x1EC0, 0x1EC1, 0x1EC2, 0x1EC3, 0x1EC4, 0x1EC5, 0x1EC6, 0x1EC7,
    #0x1EC8, 0x1EC9, 0x1ECA, 0x1ECB, 
    #0x1ECE, 0x1ECF,
    #0x1ED0, 0x1ED1, 0x1ED2, 0x1ED3, 0x1ED4, 0x1ED5, 0x1ED6, 0x1ED7, 0x1ED8, 0x1ED9,
    #0x1EDA, 0x1EDB, 0x1EDC, 0x1EDD, 0x1EDE, 0x1EDF, 0x1EE0, 0x1EE1, 0x1EE2, 0x1EE3,
    #0x1EE4, 0x1EE5, 0x1EE6, 0x1EE7,
    #0x1EE8, 0x1EE9, 0x1EEA, 0x1EEB, 0x1EEC, 0x1EED, 0x1EEE, 0x1EEF, 0x1EF0, 0x1EF1,
    #0x1EF4, 0x1EF5, 0x1EF6, 0x1EF7])
    self.dup_glyph_by_id(REFS.keys,bdf)
  end
end

class Glyph
  attr_reader :swidth, :dwidth, :bbx, :bitmap
  attr_accessor :encoding, :name
  def initialize(buf)
    done = false
    bitmap_phase = false
    @bitmap = []
    while (!buf.empty && !done) do
      items = buf.line.split(/ /)
      if bitmap_phase
	if items[0] == 'ENDCHAR'
	  done = true
	else
          @bitmap << buf.line
	end
      else
        @name = items[1].dup if items[0] == 'STARTCHAR'
        @encoding = items[1].to_i if items[0] == 'ENCODING'
        @swidth = items.dup if items[0] == 'SWIDTH'
        @dwidth = items.dup if items[0] == 'DWIDTH'
        @bbx = items.dup if items[0] == 'BBX'
        bitmap_phase = true if buf.line == 'BITMAP'
      end 
      buf.swallow
    end
    self
  end

  def write(io)
    io.puts "STARTCHAR #@name"
    io.puts "ENCODING #@encoding"
    io.puts @swidth.join(' ')
    io.puts @dwidth.join(' ')
    io.puts @bbx.join(' ')
    io.puts "BITMAP"
    @bitmap.each {|b| io.puts b}
    io.puts "ENDCHAR"
  end
end

#$buf = open_bdf('ter-u16n.bdf')
#$bdf = BDF.new($buf)
#require 'pp'
#$bdf.write(STDOUT)
#a = BDF.new('zz')
#b = BDF.new('ter-u12n.bdf')
#a.copy_all(b)
#c = File.open('zzo','w')
#a.write(c)
#c.close
